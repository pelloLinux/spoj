#include <stdio.h>
#include <math.h>

int main ()
{
   int max;
   long num;
   float k;
   int k1;
   int asmatu;
   long karratua;
   float y;

   scanf("%d", &max);
   //long lista[max];
   int i;
   for(i=0; i<max; i++)
   {
     scanf("%d", &num);
     if(num%2 != 0)
     {
       k = (num-1)/4;
       if(fmod(k, 1) == 0.0)
          printf("Yes\n");
       else
          printf("No\n");
     }
     else
     {
       k1 = 0;
       asmatu = 0;
       while(asmatu==1 && k1 <= sqrt(num))
       {
         karratua = k1*k1;
         y = sqrt(num-karratua);
          if(fmod(y, 1) == 0.0)
            asmatu =1;
          else
            k1++;
       }
       if(asmatu == 1)
          printf("Yes\n");
       else
          printf("No\n");
     }
   }

}
