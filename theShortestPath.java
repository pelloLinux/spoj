import java.util.*;
import java.lang.*;

class Main
{

	public static void main (String[] args) throws java.lang.Exception
	{
		Scanner in = new Scanner(System.in);
		int s=Integer.parseInt(in.nextLine());//tests

		for(int w=0; w<s; w++)
		{
			int n=Integer.parseInt(in.nextLine());//citys
			String[] names = new String[n+1];
			LinkedList<int[]> erpinenBideak = new LinkedList<int[]>();//Nik asmatutako datu mota.

			for(int j=0; j<n; j++)
			{
				names[j+1] = in.nextLine();
				int neigh = Integer.parseInt(in.nextLine());//kontaktuak
				int[] bideak = new int[2*neigh];
				
				for(int i=0; i<neigh; i++)
				{
					String konekzioa = in.nextLine();
					String[] strs = konekzioa.split(" ");
					int ind= Integer.parseInt(strs[0]);
					int kost=Integer.parseInt(strs[1]);
					
					bideak[2*i] = ind;//bidea dago bi erpinen artean
					bideak[2*i+1] = kost; //hau da bere kostua
				}
				erpinenBideak.add(bideak);
			}

			int r=Integer.parseInt(in.nextLine());//bide kopurua
			int[] nondik = new int[r];
			int[] nora = new int[r];
			for(int j=0; j<r; j++){//izenakin emateulako informazioa

				String konekzioa = in.nextLine();
				String[] strs = konekzioa.split(" ");
				for(int i=1; i<n+1; i++){
					if(names[i].compareTo(strs[0])==0)nondik[j]=i;
					if(names[i].compareTo(strs[1])==0)nora[j]=i;
				}
			}

			for(int j=0; j<r; j++){

				System.out.println(dikstra(n, nondik[j], nora[j], erpinenBideak));
			}

			if(w!=s-1)
				in.nextLine();
		}
		in.close();

	}
	private static int dikstra(int n, int nondik, int nora, LinkedList<int[]> erpinenBideak){

		if(nondik == nora)return 0;//ez baitu ezer egin behar

		int min;
		int[] minKost = new int[n+1];
		int[] hautagaiak = new int[n+1];
		hautagaiak[nondik] = -1;//bertatik hasi behar baita.
		
		for(int i=1; i<n+1; i++){
			minKost[i] = -1;//-1 ezin bada iritsi.
		}
		for(int i=0; i<erpinenBideak.get(nondik-1).length; i+=2)//irteerako herriaren bideak
		{
			minKost[erpinenBideak.get(nondik-1)[i]]=erpinenBideak.get(nondik-1)[i+1];
		}
		
		while(true)
		{
			min = 100000;
			for(int i=1; i<n+1; i++)//txikiena aurkitu
			{
				if(minKost[i]<min && minKost[i]>=0 && hautagaiak[i]!=-1)
					min = i;
			}
			hautagaiak[min]=-1;//hautagaietatik kendu			
			if(min == nora)break;
			else
			{
				for(int i=0; i<erpinenBideak.get(min-1).length; i+=2){
							
					if(minKost[erpinenBideak.get(min-1)[i]] > minKost[min]+erpinenBideak.get(min-1)[i+1] || minKost[erpinenBideak.get(min-1)[i]]==-1)
						minKost[erpinenBideak.get(min-1)[i]] = minKost[min]+erpinenBideak.get(min-1)[i+1];
				}
			}
		}

		return minKost[nora];
	}
}


